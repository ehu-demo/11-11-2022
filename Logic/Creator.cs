﻿using System;

namespace Logic
{
    public static class Creator
    {
        public static string Create(int borderValue, int count, string firstWord, string secondWord, int a, int b)
        {
            string result = string.Empty;//vs ""

            for (int number = 1; number <= borderValue; number++)
            {
                string value = CreateValue(firstWord, secondWord, a, b, number);
                string separator = CreateSeparator(borderValue, count, number);
                //result = result + $"{value}{separator}";
                // vs
                result += $"{value}{separator}";
            }

            return result;
        }

        private static string CreateValue(string firstWord, string secondWord, int a, int b, int number)
            => (number % a) switch
            {
                0 when number % b == 0 => $"{firstWord}{secondWord}",
                0 => firstWord,
                _ => number % b == 0 ? secondWord : $"{number}",
            };

        private static string CreateSeparator(int borderValue, int count, int number)
        {
            return (number % count) switch
            {
                0 when number == borderValue => string.Empty,
                0 => $", {Environment.NewLine}",
                _ => number == borderValue ? string.Empty : ", ",
            };
        }
    }
}