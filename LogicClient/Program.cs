﻿using static System.Console;
using static Logic.Creator;

static class Program
{
    public static void Main()
    {

        char @continue = 'y';

        do
        {
            int border = InputValue(nameof(border));

            int count = InputValue(nameof(count));

            Write("Enter first word: ");
            string firstWord = ReadLine();
            Clear();

            Write("Enter second word: ");
            string secondWord = ReadLine();
            Clear();

            int a = InputValue(nameof(a));

            int b = InputValue(nameof(b));

            WriteLine(Create(border, count, firstWord, secondWord, a, b));

            WriteLine();
            WriteLine("Press 'y' or 'Y' if want to continue.");
            @continue = char.Parse(ReadLine());
            Clear();

        } while (@continue is 'y' or 'Y');

        WriteLine("Finished!");
        ReadKey();
    }

    private static int InputValue(string name)
    {
        while (true)
        {
            Write($"Enter {name} > 0: ");
            string input = ReadLine();
            if (int.TryParse(input, out var value) && value > 0)
            {
                return value;
            }

            WriteLine($"Invalid {name}. Try again.");
        }
    }
}